/*
 * Copyright (c) 2018 Lars Thrane A/S
 * SPDX-License-Identifier: GPL-2.0
 *
 * Telnet server - Telnet protocol handling
 */

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "telnet.h"
#include "telnet_param.h"


/* Telnet commands */
#if 0
static const char *const tn_cmd[] = {
    "SE", "NOP", "MARK", "BRK", "IP", "AO", "AYT", "EC",
    "EL", "GA", "SB", "WILL", "WONT", "DO", "DONT", "IAC"
};
#endif

#define OPT_NACK    0           /* Reject      (DONT/WONT)      */
#define OPT_IGNORE  1           /* Ignore the command (?)       */
#define OPT_ACK     2           /* Acknowledge (DO  /WILL)      */

/* Telnet option support */
static const unsigned char tn_opt[256] = {
/* *INDENT-OFF* */
    [TNO_BINARY] = OPT_ACK,
    [TNO_SUPP_GA] = OPT_ACK,
//  [TNO_TTYP]   = OPT_NACK,
//  [TNO_NAWS]   = OPT_NACK,
//  [TNO_NEO]    = OPT_NACK,
    [TNO_CPCO]   = OPT_ACK,
//  [TNO_KERMIT] = OPT_NACK,
/* *INDENT-ON* */
};


static void _telnet_handle_req(tn_ctx_t * tcc, int cmd, int opt)
{
    char            buf[3];

    if (tcc->telnet_ack[opt] > 0)
        return;

    switch (tn_opt[opt])
    {
    default:
    case OPT_IGNORE:
        return;
    case OPT_ACK:
        cmd = TNC_FD_DO;
        break;
    case OPT_NACK:
        cmd = TNC_FE_DONT;
        break;
    }

    buf[0] = TNC_FF_IAC;
    buf[1] = cmd;
    buf[2] = opt;

    tcc->telnet_ack[opt]++;

    tcc->srv_tx(tcc->cctx, buf, 3);
}

static void _telnet_handle_resp(tn_ctx_t * tcc, int cmd, int opt)
{
    if (opt == TNO_CPCO)
    {
        if (cmd == TNC_FD_DO)
            tcc->mode_rfc2217 = 1;
    }
}

void telnet_set_opt(tn_ctx_t * tcc, int opt, int prm, const void *val, int len)
{
    unsigned char   buf[128], *pu = buf;
    const unsigned char *pv;
    int             i;

    *pu++ = TNC_FF_IAC;
    *pu++ = TNC_FA_SB;

    *pu++ = opt;
    *pu++ = prm;
    if (len > 0 && len <= 32)
    {
        pv = val;
        for (i = 0; i < len; i++)
        {
            *pu++ = *pv;
            if (*pv++ == TNC_FF_IAC)
                *pu++ = TNC_FF_IAC;
        }
    }

    *pu++ = TNC_FF_IAC;
    *pu++ = TNC_F0_SE;

    len = pu - buf;

    tcc->srv_tx(tcc->cctx, buf, len);
}

static void _telnet_handle_opt(tn_ctx_t * tcc, int opt,
                               unsigned char *pu, int nd)
{
    switch (opt)
    {
    default:
        break;
    case TNO_CPCO:             /* RFC 2217 */
        tcc->mode_rfc2217 = 1;
        telnet_rfc2217_handle_opt(tcc, opt, pu, nd);
        break;
    }
}

int telnet_rx(tn_ctx_t * tcc, char *buf, int *plen)
{
    char           *s, *p;
    unsigned char  *pu;
    int             len, rem;
    int             np, nd;
    int             b1, b2;

    len = *plen;
    tcc->bytes_rx += len;

    if (len < (int)tcc->off_rx)
        tcc->off_rx = 0;

    for (s = buf + tcc->off_rx, rem = len - tcc->off_rx; rem > 0; s = p)
    {
        p = memchr(s, TNC_FF_IAC, rem);
        if (!p)
            break;

        /* We have IAC (= 0xff) */
        np = rem - (p - s);     /* N. chars available incl. IAC */
        if (np < 2)
            return 1;

        pu = (unsigned char *)p;
        b1 = pu[1];
        switch (b1)
        {
        default:
        case TNC_F0_SE:
        case TNC_F1_NOP:
        case TNC_F2_MARK:
        case TNC_F3_BRK:
        case TNC_F4_IP:
        case TNC_F5_AO:
        case TNC_F6_AYT:
        case TNC_F7_EC:
        case TNC_F8_EL:
        case TNC_F9_GA:
            nd = 2;             /* Drop 2 bytes */
            goto tn_drop;

        case TNC_FB_WILL:
        case TNC_FC_WONT:
        case TNC_FD_DO:
        case TNC_FE_DONT:
            if (np < 3)
                goto tn_more;   /* Need more */
            tcc->mode_telnet = 1;       /* Any such sequence activates TN */
            b2 = pu[2];
            if (b1 == TNC_FB_WILL)
                _telnet_handle_req(tcc, b1, b2);
            else if (b1 == TNC_FD_DO)
                _telnet_handle_resp(tcc, b1, b2);
            nd = 3;             /* Drop 3 bytes */
            goto tn_drop;

        case TNC_FA_SB:
            for (nd = 2; nd < np - 1; nd++)
            {
                if (pu[nd] == TNC_FF_IAC && pu[nd + 1] == TNC_F0_SE)
                {
                    nd -= 2;
                    if (nd <= 0)
                        break;
                    b2 = pu[2];
                    tcc->mode_telnet = 1;       /* Any such sequence activates TN */
                    _telnet_handle_opt(tcc, b2, pu + 3, nd - 1);
                    nd += 4;
                    goto tn_drop;
                }
            }
            if (np < 16)
                goto tn_more;   /* Not done */
            /* This does not look right */
            rem = 0;            /* Quit loop */
            break;

        case TNC_FF_IAC:
            p++;
            nd = 1;             /* Drop 1 bytes */
            goto tn_drop;

          tn_drop:
            if (!tcc->mode_telnet)
                goto tn_break;
            rem -= nd + (p - s);
            len -= nd;
            memmove(p, p + nd, rem);
            break;

          tn_break:
            rem = 0;            /* Quit loop */
            break;

          tn_more:             /* Request more data */
            tcc->off_rx = len - np;
            *plen = len;
            return 1;
        }
    }

    tcc->off_rx = 0;
    *plen = len;

    return 0;
}

int telnet_tx(tn_ctx_t * tcc, const char *buf, int len)
{
    const char     *s, *p;
    int             rem, nd;

    for (s = buf, rem = len; rem > 0; s = p)
    {
        if (tcc->mode_telnet)
            p = memchr(s, TNC_FF_IAC, rem);
        else
            p = NULL;
        if (!p)
        {
            /* Send the rest */
            nd = rem;
            tcc->srv_tx(tcc->cctx, s, nd);
        }
        else
        {
            nd = p - s + 1;
            tcc->srv_tx(tcc->cctx, s, nd);
            tcc->srv_tx(tcc->cctx, p, 1);       /* One TNC_FF_IAC */
            p++;
        }
        rem -= nd;
    }

    return 0;
}

int telnet_open(tn_ctx_t * tcc)
{
    time_t          t = time(NULL);;
    char            buf[3];
    int             res;

    buf[0] = TNC_FF_IAC;
    buf[1] = TNC_FB_WILL;
    buf[2] = TNO_CPCO;

    tcc->srv_tx(tcc->cctx, buf, 3);

    while (1)
    {
        res = tcc->srv_rx(tcc->cctx, 1000);
        if (res <= 0)
            return -1;
        else if (tcc->mode_rfc2217)
            return 0;
        else if (t - time(NULL) > 2)
            return -1;
    }
}

tn_ctx_t       *telnet_ctx_init(void *cctx, srv_tx_f * ftx,
                                srv_rx_f * frx, srv_ms_f * fms)
{
    tn_ctx_t       *tcc;

    tcc = calloc(1, sizeof(tn_ctx_t));
    tcc->cctx = cctx;
    tcc->srv_tx = ftx;          /* Write to server */
    tcc->srv_rx = frx;          /* Read from server */
    tcc->srv_ms = fms;          /* Set modem state */

    return tcc;
}
